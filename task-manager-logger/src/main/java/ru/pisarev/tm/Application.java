package ru.pisarev.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IReceiverService;
import ru.pisarev.tm.listener.LogMessageListener;
import ru.pisarev.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull final IReceiverService receiverService = new ActiveMQConnectionService();
        receiverService.receive(new LogMessageListener());
    }

}