package ru.pisarev.tm.api.service;

import javax.jms.TextMessage;

public interface ILoggingService {

    void writeLog(TextMessage textMessage);

}
