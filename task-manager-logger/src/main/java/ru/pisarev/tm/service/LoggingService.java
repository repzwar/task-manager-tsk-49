package ru.pisarev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.service.ILoggingService;

import javax.jms.TextMessage;
import java.io.File;
import java.io.FileWriter;

import static ru.pisarev.tm.constant.LogFileName.*;

public class LoggingService implements ILoggingService {

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final TextMessage textMessage) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String message = textMessage.getText();
        @NotNull final ObjectNode node = objectMapper.readValue(message, ObjectNode.class);
        if (!node.has("className")) return;
        @Nullable final String className = node.get("className").asText();
        @Nullable final String fileName = getFileName(className);
        if (fileName == null) return;
        @NotNull File file = new File(fileName);
        file.getParentFile().mkdir();
        @NotNull final FileWriter fileOutputStream = new FileWriter(file, true);
        fileOutputStream.write(message);
        fileOutputStream.close();
    }

    @Nullable
    private String getFileName(@Nullable final String className) {
        if (className == null) return null;
        switch (className) {
            case "SessionRecord":
            case "SessionGraph":
                return SESSION_LOG;
            case "UserRecord":
            case "UserGraph":
                return USER_LOG;
            case "TaskRecord":
            case "TaskGraph":
                return TASK_LOG;
            case "ProjectRecord":
            case "ProjectGraph":
                return PROJECT_LOG;
            default:
                return null;
        }
    }
}
