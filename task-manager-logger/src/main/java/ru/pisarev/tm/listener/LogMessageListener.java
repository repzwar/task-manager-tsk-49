package ru.pisarev.tm.listener;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.ILoggingService;
import ru.pisarev.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    final ILoggingService loggingService = new LoggingService();

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            loggingService.writeLog((TextMessage) message);
        }
    }

}
