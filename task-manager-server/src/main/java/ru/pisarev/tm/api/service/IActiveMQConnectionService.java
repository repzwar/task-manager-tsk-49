package ru.pisarev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IActiveMQConnectionService {

    void sendMessage(@NotNull String json);

}
