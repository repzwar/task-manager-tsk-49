package ru.pisarev.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@XmlRootElement
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class EntityWrapperForJson {

    private final Object record;

    private final String formattedDate;

    private final String className;

    private final String action;

}
