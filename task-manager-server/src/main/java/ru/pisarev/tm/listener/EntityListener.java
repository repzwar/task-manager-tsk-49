package ru.pisarev.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pisarev.tm.api.service.IActiveMQConnectionService;
import ru.pisarev.tm.dto.EntityWrapperForJson;
import ru.pisarev.tm.service.ActiveMQConnectionService;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EntityListener {

    @NotNull
    final IActiveMQConnectionService service;

    public EntityListener() {
        service = new ActiveMQConnectionService();
    }

    @PostLoad
    public void onPostLoad(@Nullable final Object record) {
        sendMessage(record, "LOAD");
    }

    @PrePersist
    public void onPrePersist(@Nullable final Object record) {
        sendMessage(record, "START PERSIST");
    }

    @PostPersist
    public void onPostPersist(@Nullable final Object record) {
        sendMessage(record, "FINISH PERSIST");
    }

    @PreUpdate
    public void onPreUpdate(@Nullable final Object record) {
        sendMessage(record, "START UPDATE");
    }

    @PostUpdate
    public void onPostUpdate(@Nullable final Object record) {
        sendMessage(record, "FINISH UPDATE");
    }

    @PreRemove
    public void onPreRemove(@Nullable final Object record) {
        sendMessage(record, "START REMOVE");
    }

    @PostRemove
    public void onPostRemove(@Nullable final Object record) {
        sendMessage(record, "FINISH REMOVE");
    }

    @SneakyThrows
    private void sendMessage(
            @Nullable final Object record,
            @NotNull final String message
    ) {
        @NotNull final EntityWrapperForJson wrapper = new EntityWrapperForJson(
                record,
                new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()),
                record == null ? null : record.getClass().getSimpleName(),
                message
        );
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(wrapper);
        service.sendMessage(json);
    }

}
