package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.pisarev.tm.api.service.IActiveMQConnectionService;

import javax.jms.*;

import static ru.pisarev.tm.constant.ActiveMQConst.STRING;
import static ru.pisarev.tm.constant.ActiveMQConst.URL;

public class ActiveMQConnectionService implements IActiveMQConnectionService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ActiveMQConnectionService() {
        connectionFactory = new ActiveMQConnectionFactory(URL);
    }

    @Override
    @SneakyThrows
    public void sendMessage(@NotNull final String json) {

        @NotNull final Connection connection = connectionFactory.createConnection();

        connection.start();

        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(STRING);

        final MessageProducer producer = session.createProducer(destination);

        final TextMessage message = session.createTextMessage(json);

        producer.send(message);
    }

}
